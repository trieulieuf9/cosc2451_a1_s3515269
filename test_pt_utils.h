#ifndef TEST_PT_UTILS
#define TEST_PT_UTILS

void test_shuffle_int_array(int array[], int length);

void test_linear_search(int a[], int length, int target);

void test_binary_search(int a[], int length, int target);

#endif