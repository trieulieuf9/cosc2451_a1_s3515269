#include <stdio.h>
#include <stdbool.h>

int main(){
	int n = 0;
	int count = 1;  // count the position
	int times = 0;  // how many times of occurrences
	int first_int;
	int input;

	scanf("%i", &first_int);

	while(n != -1){
		n = scanf("%i", &input);

		if(n != 0){
			if(first_int == input){
				printf("Number %i was found at position: %i\n", first_int, count);
				times++;
			}
		}
		else{  // flush memory when input is wrong
			int c;
			while((c = getchar()) != '\n')
				printf("");
		}
		count++;
	}
	printf("Number %i was found %i times\n", first_int, times);

	return 0;
}