#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "pt_utils.h"
// argv[1] = length of array
// argv[2] = the random number generator seed
// argv[3] = the number of times each search function 
// should be executed
// 
// check if argv[1], 2 ,3 not null, if null, do nothing

int main(int argc, char * argv[]) {

    if(argc == 4){
        // begin setting up the timing function performance
        int nbits = 10;
        int length = atoi(argv[1]);
        printf("%d elements\n", length);
        srand(atoi(argv[2]));
        int repetition_times = atoi(argv[3]);
        
        int * a = gen_rand_int_array(length, nbits);
        int * clone = clone_int_array(a, length);
        int * sorted_clone = clone_int_array(a, length); //used for binary search

        selection_sort(sorted_clone, length);

        // Done setting up
        // Begin timing
        float tim = 0; // keyword time is reserved    
        long clock_micros = 0;
        float clock_ms = 0;
        float clock_s = 0;
        // timing linear search
        printf("%s\n", "-----------Linear Search-----------");
        for(int i = 0; i < repetition_times; i++){
            clock_t start_c = clock();
            linear_search(clone, length, pt_rand(nbits));
            clock_t end_c = clock();

            clock_ms += (end_c - start_c) / 1000.0;

            if(i + 1 == repetition_times){
                clock_ms = clock_ms / repetition_times;

                printf("Clock (ms): %f\n", clock_ms);
            }        
        }
        // timing binary search
        tim = 0; 
        clock_micros = 0;
        clock_ms = 0;
        clock_s = 0;
        printf("%s\n", "-----------Binary Search-----------");
        for(int i = 0; i < repetition_times; i++){
            clock_t start_c = clock();
            binary_search(sorted_clone, length, pt_rand(nbits));
            clock_t end_c = clock();

            clock_ms += (end_c - start_c) / 1000.0;

            if(i + 1 == repetition_times){
                clock_ms = clock_ms / repetition_times;

                printf("Clock (ms): %f\n", clock_ms);
            }        
        }
        free(sorted_clone);
        free(clone);
        free(a); 
    }
}

