#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "test_pt_utils.h"
#include "pt_utils.h"

int main(){
    int array[] = {2, 4, 8, 16, 32, 64, 108};
    int length = sizeof(array) / sizeof(array[0]);  // length of array
    int target = 64;

    test_linear_search(array, length, target);
    test_binary_search(array, length, target);
    test_shuffle_int_array(array, length);
    
    return 0;
}

void test_shuffle_int_array(int array[], int length){
	printf("-------Array Shuffle--------\n");
	printf("before shuffle array:\n");
	print_int_array(array, length);
	shuffle_int_array(array, length);
	printf("before shuffle array:\n");
    print_int_array(array, length);
}

void test_linear_search(int a[], int length, int target){
	printf("-------Linear Search--------\n");
	printf("target array:\n");
	print_int_array(a, length);
	printf("target number:\n%d", target);
	printf("\nfunction output: \n%d", linear_search(a, length, target));
}

void test_binary_search(int a[], int length, int target){
	printf("\n-------Binary Search--------\n");
	printf("target array:\n");
	print_int_array(a, length);
	printf("target number:\n%d", target);
	printf("\nfunction output: \n%d\n", binary_search(a, length, target));
}
