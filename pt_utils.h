#ifndef PT_UTILS
#define PT_UTILS

// implement the binary search algorithm: the goal is to find the target 
// in array a with the given length. Assume the array is sorted. 
// It should return the position (or index), where the first occurrence of target is found, 
// or -1 if not found. Add code to your test program to test this function.
int binary_search(int a[], int length, int target);

// implement the linear search algorithm: the goal is to find the target
//  in array a with the given length. Assume the array is sorted. 
//  It should return the position (or index), where the first occurrence 
//  of target is found, or -1 if not found. Add code to your test program to 
//  test this function.
int linear_search(int a[], int length, int target);

/* swap 2 values that 2 parameter pointers point to
   without allocate extra memory
*/
void swap(int *pointer1, int *pointer2);

void selection_sort(int a[], int length);

// generate a random number in range min-max(exclusive)
int random_in_range(int min, int max);

/*
It should use Fisher and Yates’ algorithm or Knuth’s algorithm to shuffle the elements of the array in-place. 
It should not allocate any extra memory for the array or for a copy of the array.
*/
void shuffle_int_array(int a[], int length);
/** Generate a random int between 0 and 2^(nbits-1)
  * @param nbits the number of bits to use
  */
int pt_rand(int nbits);

/** Generate an array containing random int.
  * The array will be allocated on the heap.
  * @param length the length of the array
  * @param nbits the number of bits to pass to pt_rand
  */
int * gen_rand_int_array(int length, int nbits);

/** Copy the source array into the destination.
  * Assumes there is enough space in the destination to hold everything.
  * @param src the source array
  * @param dst the destination array
  * @param length the number of elements to copy
  */
void copy_int_array(int src[], int dst[], int length);

/** Clone an array.
  * The cloned array will be allocated on the heap.
  * @param a the array to clone
  * @param length the length of the array
  */
int * clone_int_array(int a[], int length);

/** Print an array of int.
  * @param a the array to print
  * @param length the length of the array
  */
void print_int_array(int a[], int length);

void selection_sort(int a[], int length);


#endif
