clang -Wall  -c smallest.c
clang -Wall  -c positions.c
clang -Wall  -o smallest smallest.o
clang -Wall  -o positions positions.o

/home/bit/Documents/COSC2451_2016A/list_repos/cosc2451_a1_s3515269
Contributors:

CMakeLists.txt:
cmake_minimum_required(VERSION 3.3)
project(cosc2451_a1_s3515269)

set(CMAKE_C_COMPILER clang)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "$ {CMAKE_CURRENT_SOURCE_DIR}/bin")

set(SOURCE_FILES smallest.c position.c)
add_executable(smallest smallest.c position position.c);

-------------------------------------------------------------------------
Testing  ./smallest
-------------------------------------------------------------------------
1a.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 2
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 2
---
> The smallest number is: 2

1b.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 2
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 2
---
> The smallest number is: 2

1c.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 32766
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 32766
---
> Empty input! No numbers found!

1e.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : -5
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : -5
---
> The smallest number is: -5

1f.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 2
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 2
---
> The smallest number is: 2

1g.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 5
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 5
---
> The smallest number is: 5

1h.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 2
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 2
---
> The smallest number is: 2

1i.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 2
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 2
---
> The smallest number is: 2

1j.txt
********
The smallest number is : John Cena !!!
Just Kidding :)))

The smallest number is : 5
1,4c1
< The smallest number is : John Cena !!!
< Just Kidding :)))
< 
< The smallest number is : 5
---
> The smallest number is: 5



-------------------------------------------------------------------------
Testing  ./positions
-------------------------------------------------------------------------
2a.txt
********
Number 2 was found at position: 2
Number 2 was found at position: 7
Number 2 was found 2 times
3c3
< Number 2 was found 2 times
---
> Number 2 was found 2 times.

2b.txt
********
Number 2 was found at position: 2
Number 2 was found at position: 7
Number 2 was found 2 times
3c3
< Number 2 was found 2 times
---
> Number 2 was found 2 times.

2c.txt
********
Number 0 was found 0 times
1c1
< Number 0 was found 0 times
---
> Empty input! No numbers found!

2f.txt
********
Number 8 was found 0 times
1c1
< Number 8 was found 0 times
---
> Number 8 was found 0 times.

2g.txt
********
Number 2 was found at position: 7
Number 2 was found 1 times
1,2c1,2
< Number 2 was found at position: 7
< Number 2 was found 1 times
---
> Number 2 was found at position: 6
> Number 2 was found 1 times.

2h.txt
********
Number 2 was found at position: 2
Number 2 was found at position: 8
Number 2 was found 2 times
2,3c2,3
< Number 2 was found at position: 8
< Number 2 was found 2 times
---
> Number 2 was found at position: 7
> Number 2 was found 2 times.

2i.txt
********
Number 2 was found at position: 2
Number 2 was found at position: 7
Number 2 was found 2 times
3c3
< Number 2 was found 2 times
---
> Number 2 was found 2 times.

2j.txt
********
Number 2 was found at position: 2
Number 2 was found 1 times
2c2
< Number 2 was found 1 times
---
> Number 2 was found 1 times.



