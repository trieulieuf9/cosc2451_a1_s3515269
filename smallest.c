#include <stdio.h>
#include <limits.h>
#include <stdbool.h>

int main(){
	int smallest = INT_MAX;
	int input;
	int n = 0;
	int c;

	//printf("Enter your Input here:\n");

	while(n != -1){//FEEDBACK: you should never use -1 directly here, you should use EOF
		n = scanf("%i", &input);

		if(n != 0){ 
			if(input < smallest){//FEEDBACK: input used before being initialised when input is completely empty
				smallest = input;
			}
		}
		else{	//getting wrong type of inputs
			while((c = getchar()) != '\n')//FEEDBACK: problem when the last line of the file doesn't end with \n
				//FEEDBACK: you should check for EOF also, as I did in class
				printf("");//FEEDBACK: completely unnecessary
		}
	}
	printf("The smallest number is : John Cena !!!\n");//FEEDBACK: very annoying extra output

	printf("Just Kidding :)))\n");

	printf("\nThe smallest number is : %i\n", smallest);

	return 0;
}