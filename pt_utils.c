#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "pt_utils.h"

// int main(){
//     int array[] = {2, 4, 8, 16, 32, 64, 108};
//     int length = sizeof(array) / sizeof(array[0]);  // length of array

//     print_int_array(array, length);
//     printf("position: %d\n",binary_search(array, length, 2));

//     //printf("position: %d\n",linear_search(array, length, 32));
//     return 0;
// }

int binary_search(int a[], int length, int target){
    int down = 0;
    int up = length - 1;
    int mid = (up + down) / 2;

    while(down <= up){
        mid = (up + down) / 2;
        if(a[mid] < target)
            down = mid + 1;
        else if (a[mid] > target)
            up = mid - 1;
        else
            return mid;
    }
    return -1;
}

int linear_search(int a[], int length, int target){
    for(int i = 0; i < length; i++){
        if(a[i] == target)
            return i;
    }
    return - 1;
}

void shuffle_int_array(int a[], int length){
    for(int i = 0; i < length; i++){
        swap(&a[i], &a[random_in_range(i, length)]);
    }
}

void selection_sort(int a[], int length) {
    int min_pos;
    
    /* find the min value in a[i] to a[length-1], and swap it with a[i] */
    for (int i = 0; i < length; i++) {
        min_pos = i;
        /* find the minimum value in what is left of the array */
        for (int j = i+1; j < length; j++) {
            if (a[j] < a[min_pos]) {
                min_pos = j;
            }
        }
        swap(&a[i], &a[min_pos]);
    }
    
}

int random_in_range(int min, int max){
    if(max == min)
        return min;
    else if (max < min)
        return rand();
    else{
        int limit = max - min;
        srand(time(NULL));
        return (rand() % limit) + min;
    }   
}

void swap(int *pointer1, int *pointer2){
    //swap 2 pointers without using extra variable
    if(*pointer1 != *pointer2){
        *pointer1 = *pointer1 + *pointer2;
        *pointer2 = *pointer1 - *pointer2; 
        *pointer1 = *pointer1 - *pointer2;
    }
}

int pt_rand(int nbits) {
    int mask;
    if (0 < nbits && nbits < sizeof(int)*8) {
        // the least significant nbits bits will be 1, others will be 0
        mask = ~(~((unsigned int) 0) << nbits); 
    }
    else {
        // the mask will be all ones
        mask = ~((unsigned int) 0);
    }
    // get the next random number and keep only nbits bits
    return rand() & mask;
}

int * gen_rand_int_array(int length, int nbits) {

    int * a = malloc(sizeof(int)*length);
    for (int i = 0; i < length; i++) {
        a[i] = pt_rand(nbits);
    }
    return a;
}

void copy_int_array(int src[], int dst[], int length) {

    for (int i = 0; i < length; i++) {
        dst[i] = src[i];
    }
}

int * clone_int_array(int a[], int length) {

    int * clone = malloc(sizeof(int)*length);
    copy_int_array(a, clone, length);
    
    return clone;
}

void print_int_array(int a[], int length) {

    for (int i = 0; i < length; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

