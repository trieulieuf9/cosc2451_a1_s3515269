#!/bin/bash
function run_tests {
    echo $line >> results_part1.txt
    echo "Testing " $exec_file >> results_part1.txt
    echo $line >> results_part1.txt

    for input in "${input_files[@]}"
    do
        echo $input >> results_part1.txt
        echo "********" >> results_part1.txt
        $exec_file < test_files_p1/$input > test_files_p1/$input.out
        cat test_files_p1/$input.out >> results_part1.txt
        diff test_files_p1/$input.out test_files_p1/$input.sol >> results_part1.txt 
        echo >> results_part1.txt
    done
    echo >> results_part1.txt
    echo >> results_part1.txt
}


cp ../Makefile .
make clean
make > results_part1.txt
echo >> results_part1.txt

pwd >> results_part1.txt
echo "Contributors:" >> results_part1.txt
cat contributors.txt >> results_part1.txt
echo >> results_part1.txt

pwd > comments_part1.txt
echo "Contributors:" >> comments_part1.txt
cat contributors.txt >> comments_part1.txt
echo >> comments_part1.txt

echo "CMakeLists.txt:" >> results_part1.txt
cat CMakeLists.txt >> results_part1.txt
echo >> results_part1.txt

echo >> results_part1.txt

line="-------------------------------------------------------------------------"
cp -R ../test_files_p1 .
input_files=( 1a.txt 1b.txt 1c.txt 1e.txt 1f.txt 1g.txt 1h.txt 1i.txt 1j.txt )
exec_file=./smallest
run_tests

input_files=( 2a.txt 2b.txt 2c.txt 2f.txt 2g.txt 2h.txt 2i.txt 2j.txt )
exec_file=./positions
run_tests

git add results_part1.txt comments_part1.txt Makefile test_files_p1 markP1.sh


