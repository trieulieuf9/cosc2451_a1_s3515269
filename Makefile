CC=clang
CFLAGS=-Wall #-g -pg #-std=c99 
SOURCES = smallest.c positions.c
OBJS = $(patsubst %.c,%.o,$(SOURCES))
EXECS = $(patsubst %.c,%,$(SOURCES))

all: $(EXECS)

$(EXECS): $(OBJS) 
	$(CC) $(CFLAGS) -o $@ $@.o

%.o: %.c
	$(CC) $(CFLAGS) -c $^

.PHONY: clean
clean:
	rm *.o 
	
.PHONY: cleanall
cleanall:
	rm *.o *~ $(EXECS)
